﻿//Написать функции для поразрядного сложения и вычитания длинных целых чисел, представленных строками символов, и тестирующую программу к ним.

#include <iostream>
#include "add_and_diff.h"
using namespace std;

int main()
{
	char num1[100] = { 0 };
	char num2[100] = { 0 };
	char add[101] = { 0 };
	char diff[100] = { 0 };
	cout << "Enter first number: ";
	cin >> num1;
	cout << "Enter second number: ";
	cin >> num2;
	func_add(num1, num2, add);	
	func_diff(num1, num2, diff);
	for (int i = 0; i < 100; i++)
	{
		if (add[i] != 0)
		{
			cout << add[i];
		}
	}
	cout << endl;
	for (int i = 0; i < 99; i++)
	{
		if (diff[i] != 0)
		{
			cout << diff[i];
		}
	}
}

