#include <iostream>
#include "add_and_diff.h"

void func_add(char num1[], char num2[], char add[])
{
	bool ed = 0;
	int i = 100;
	int len1 = strlen(num1);
	int len2 = strlen(num2);
	while ((len1 >= 0) && (len2 >= 0))
	{
		add[i] = num1[len1] + (num2[len2] - '0');
		if (ed == 1)
		{
			add[i]++;
			ed = 0;
		}
		if (add[i] > '9')
		{
			add[i] -= 10;
			ed = 1;
		}
		i--;
		len1--;
		len2--;
	}
	if (ed == 1)
	{
		add[i] = 1;
	}
	while (len1 >= 0)
	{
		add[i] += num1[len1];
		i--;
		len1--;
	}
	while (len2 >= 0)
	{
		add[i] += num2[len2];
		i--;
		len2--;
	}
	if (add[i] == 1)
	{
		add[i] = '1';
	}
}

void func_diff(char num1[], char num2[], char diff[])
{
	bool ed = 0;
	int i = 99;
	int len1 = strlen(num1);
	int len2 = strlen(num2);
	int k = len1 - len2;
	if (k >= 0)
	{
		while (len2 >= 0)
		{
			diff[i] = num1[len1] - (num2[len2] - '0');
			if (ed == 1)
			{
				diff[i]--;
				ed = 0;
			}
			if (diff[i] < '0')
			{
				if (diff[i - 1] != 0)
				{
					diff[i] += 10;
					ed = 1;
				}
				else
				{
					diff[i] = diff[i] - '0';
					diff[i] = (char)abs(diff[i]) + '0';
					diff[i - 1] = '-';
				}
			}
			i--;
			len1--;
			len2--;
		}
		if (ed == 1)
		{
			diff[i] -= 1;
		}
		while (len1 >= 0)
		{
			diff[i] += num1[len1];
			i--;
			len1--;
		}
	}
	else
	{
		while (len1 >= 0)
		{
			diff[i] = num2[len2] - num1[len1] + '0';
			if (ed == 1)
			{
				diff[i]--;
				ed = 0;
			}
			if (diff[i] < '0')
			{
				diff[i] += 10;
				ed = 1;
			}
			i--;
			len1--;
			len2--;
		}
		if (ed == 1)
		{
			diff[i] -= 1;
		}
		while (len2 >= 0)
		{
			diff[i] += num2[len2];
			i--;
			len2--;
		}
		if (diff[i + 1] == '0')
		{
			diff[i + 1] = 0;
		}
		diff[i] = '-';
	}
}